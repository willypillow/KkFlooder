# -*- coding: cp950 -*-
# This is a multi-threaded screen flooder for the MUD "The Kings of Kings", aka "KK".
# Edit the "Config" part below before you use it.
import telnetlib
import time
import sys
import random
import string
from threading import Thread

#---Config---#
# Number of threads (characters) to be opened.
threads = 2 

# The command that will be send. Remember to add a line break \n at the end.
message = "tell XXX hi\n"

# The length of the random-generated username for the characters.
namelen = 12

# The Chinese name of the characters. Remember to add a line break \n at the end.
chiname = "哇哈哈\n"

# The password of the characters. Remember to add a line break \n at the end.
passw = "localhost\n"

# The class of the characters. Remember to add a line break \n at the end.
# 1: Warrior
# 2: Mage
# 3: Priest
claz = "1\n"

# The gender of the characters. Remember to add a line break \n at the end.
# m: Male
# f: Female
gender = "m\n"

# The delay between the commands in seconds.
# Do not set this value too low as the server would complain that your commands are too frequent.
delay = 0.3
#---Config---#

# The on/off switch
stat = True

def start():
    # Connect and register
    tn = telnetlib.Telnet("kk.muds.idv.tw", 4000)
    
    tn.read_until("您的英文名字")
    tn.write("new\n")
    
    tn.read_until("請輸入您希望的英文名字")
    # Generate a random username at the chosen length
    tn.write(''.join(random.choice(string.ascii_lowercase) for _ in range(namelen)) + "\n")
    
    tn.read_until("您的中文名字")
    tn.write(chiname)
    
    tn.read_until("請設定您的密碼")
    tn.write(passw)
    
    tn.read_until("請再輸入一次您的密碼")
    tn.write(passw)
    
    tn.read_until("請選擇你的職業")
    tn.write(claz)
    
    tn.read_until("您的性別是男性(m)或女性(f)")
    tn.write(gender)
    
    # Set an alias for the command for quicker access
    tn.read_until("冒險者之家")
    tn.write("alias r " + message)
    
    # Keep running if the switch is on
    while stat:
        tn.write("r\n")
        time.sleep(delay)
        
    # Begin to delete the bots' accounts
    print "Exit..."
    
    time.sleep(1)
    tn.write("suicide\n")
    
    tn.read_until("確定的話請輸入您的密碼")
    tn.write(passw)
    
    tn.read_until("永別了")
    tn.close()

#---Main---#
for i in range(threads):
    thread = Thread(target=start, args=())
    time.sleep(1)
    thread.start()

raw_input("Press Enter to exit...")
# Set the switch to 'off'
stat = False